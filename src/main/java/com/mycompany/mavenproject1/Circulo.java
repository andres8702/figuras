package com.mycompany.mavenproject1;

/**
 *
 * @author sigmotoa
 */
public class Circulo extends FiguraGeometrica{
    
    private double radio;

    public Circulo(double radio) {
        this.radio = radio;
    }
    
    @Override
    public double area()
    {
        return Math.PI*Math.pow(radio, 2);
    }
    
    //perimetro
    @Override
    public double perimetro(){
        return Math.PI*2*radio;
    }
    
    //volumen
    @Override
    public double volumen(){
        return (4/3)*Math.PI*Math.pow(perimetro(),3); 
    }

    public double getRadio() {
        return radio;
    }

    public void setRadio(double radio) {
        this.radio = radio;
    }
    
    
}
