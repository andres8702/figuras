package com.mycompany.mavenproject1;

/**
 *
 * @author sigmotoa
 */
public abstract class FiguraGeometrica {
    
    public abstract double area();
    public abstract double perimetro();
    public abstract double volumen();
    
    @Override
    public String toString (){
        
        return "\033[31mAREA: \033[30m"+area()+"\n"+
                
               "\033[31mPERIMETRO: \033[30m"+perimetro()+"\n"+
                
               "\033[31mVOLUMEN: \033[30m"+volumen()+"\n"
                ;
                
    } 
    
        
    
    
}
