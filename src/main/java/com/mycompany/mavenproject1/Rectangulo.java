package com.mycompany.mavenproject1;

/**
 *
 * @author sigmotoa
 */
public class Rectangulo extends FiguraGeometrica{
    private double base;
    private double altura;

    public Rectangulo(double base, double altura) {
        this.base = base;
        this.altura = altura;
    }

    @Override
    public double area(){
    return base*altura;
    }
    
    //perimetro
    @Override
    public double perimetro(){
        return (base*2)+(altura*2);
    }
    
    //volumen
    @Override
    public double volumen(){
        return Math.pow(area(),3);
    }
    

    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }
}
